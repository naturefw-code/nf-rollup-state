import { createApp } from 'vue'
import App from './App.vue'

// import createStore from './store-nf'

// 在 template 里面可以使用 $state 获取全局状态
// import { nfState } from '../lib/main'

import { createPinia } from 'pinia'

const pinia = createPinia()

// 注册全局状态
// createStore()

// 简易路由
import router from './router'

// UI库
import ElementPlus from 'element-plus'
// import 'element-plus/lib/theme-chalk/index.css'
// import 'dayjs/locale/zh-cn'
import zhCn from 'element-plus/es/locale/lang/zh-cn'

// 二次封装
import { nfElementPlus } from '@naturefw/ui-elp'

createApp(App)
  .use(router)
  .use(ElementPlus, { locale: zhCn, size: 'small' }) // UI库
  .use(nfElementPlus) // 二次封装的组件
  // .use(nfState) // template 里面可以使用 $state 获取全局状态
  .use(pinia)
  .mount('#app')
