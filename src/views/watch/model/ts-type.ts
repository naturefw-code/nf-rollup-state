
export interface Person {
  name: string,
  age: number
}

export type Person1 = {
  name: string,
  age: number
}