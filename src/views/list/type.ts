import type { Ref, ShallowRef, UnwrapRef } from 'vue'


/**
 * 分页状态
 */
export type TPagerState = {
  pagerSize: number, // 一页的记录数
  count: number, // 总数
  pagerIndex: number // 当前页号
}

/*
 * 字段名作为key的名称，查询关键字放数组里面
 */
export type TQueryKey2 = {
  [key: string]: [string, string?] | number[]
}

/*
 * 字段名作为 key 的名称，查询关键字放数组里面
 * 一个关键字：colName：[101, xxx]
 * 两个关键字：colName：[103, 开始, 结束] // 范围查询
 * 集合查询：colName：[109, [1,2,3]] 
 */
export type TQueryMini = {
  // 字段名作为key，查询方式，一个关键字 | 集合查询，范围查询的第二个关键字，
  [key: string]:
      [ number, string] // 1
    | [ number, string, string] // 2
    | [ number, number[]] // 3
}

/**
 * 查询状态
 */
export type TQueryState = {
  mini: TQueryMini, // 查询条件的精简形式
  array: Array<{colName:string, value:string}>, // 查询条件的对象形式
}


/**
 * 列表里选择的记录
 */
export type IGridSelection<T extends object> = {
  dataId: string | number, // 单选ID number 、string
  row: T, // 单选的数据对象 {}
  dataIds: Array<string | number>, // 多选ID []
  rows: T[] // 多选的数据对象 []
}

/**
 * 列表 controller 用的状态
 */
export type TListState<T extends object> = {
  // 加载数据
  // loadData: (isReset: boolean) => void
  selection: IGridSelection<T>,
  dataList: ShallowRef<Array<T>>, // 数据列表
  query: TQueryState, // 查询条件
  pagerState: TPagerState, // 分页信息
}

/**
 * 增删改查的状态
 */
export type TCRUDState = {
  /**
   * 添加次数变更后，更新列表数据
   */
  addCount: number,
  /**
   * 修改次数变更后，更新列表数据
   */
  updateCount: number,
  /**
   * 删除次数变更后，更新列表数据
   */
  deleteCount: number,
  /**
   * 各种弹窗是否显示的状态
   */
  dialog: {
    /**
     * 添加数据的弹窗
     */
    addShow: boolean,
    /**
     * 修改数据的弹窗
     */
    updateShow: boolean,
    /**
     * 子模块的弹窗
     */
    subShow: boolean,
    // 可以扩展其他弹窗
    [key: number]: number
  }
}

/**
 * 加载数据的函数，返回的结构，用泛型表示列表数据的类型
 */
export type TResource<T> = {
  allCount: number, 
  list: Array<T>
}

/**
 * 做一个简单的企业信息作为演示
 */
export type TCompany = {
  id: string | number
  name: string, // 公司名称
  address: string, // 地址
  telephone: string // 电话
}
