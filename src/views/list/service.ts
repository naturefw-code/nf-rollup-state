import type { TListState, TQueryState, TPagerState, TCompany, TResource } from './type'

  // 获取数据，模拟一下
export default async (query: TQueryState, pagerInfo: TPagerState) => {
  // 根据参数获取数据
  // console.log(query, pagerInfo)
  
  const q = { // query.findValue
    name: query.mini.name?query.mini.name[1]:'',
    address: query.mini.address?query.mini.address[1]:'',
    telephone: query.mini.telephone?query.mini.telephone[1]:''
  }

  const re: TResource<TCompany> = {
    allCount: 1000,
    list: [] as TCompany[]
  }

  for (let i=0; i<10; i++) {
    re.list.push({
      id: pagerInfo.pagerIndex * 10 + i,
      name: q.name? `查询条件：${q.name}-- ${pagerInfo.pagerIndex + i}`: `数据测试：${pagerInfo.pagerIndex} -- ${i}`,
      address: `xxx省xx市区--${i}` + q.address,
      telephone: `139${i}${i + 1}${i + 4}${i}` + q.telephone
    })
  }
  return re
}
