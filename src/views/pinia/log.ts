
import * as pinia  from 'pinia'
import { watch } from 'vue'

export default (store: any) => {

  let isAction = false

  watch(store, (v) => {
    console.log('err--v:', v)

  },
  {
    flush: 'sync', // 'pre' | 'post' | 'sync',
    onTrigger(event) {
      if (!isAction) {
        const err = new Error('记录调用函数')
        console.log('err:', err)
        console.log('onTrigger--event:', event)
      }
    },
  })

  store.$onAction((e: any) => {
    console.log('action 的参数：', e.args)
    const err = new Error('记录调用函数--onAction')
    console.log('err:', err)
    isAction = true
    e.after(() => {
      isAction = false
    })
  })
   
}