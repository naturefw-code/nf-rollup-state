import { defineStore } from 'pinia'
import { reactive, ref } from 'vue'

interface Person {
  isLogin: boolean,
  name: string, //
  age: number,
  info: {name: string},
  roles: Array<string>
}

const key = Symbol('pnian')
const key1 = 'objectTest11'
export const usePersonStore = defineStore(key1, {
  state: (): Person => {
    return {
      isLogin: true,
      name: 'jyk999',
      age: 18,
      info: {
        name: '第二层'
      },
      roles: [
        '11',
        '22'
      ]
    }
  },
  // 也可以这样定义状态
  // state: () => ({ count: 0 })
  actions: {
    nameAction(name: string) {
      this.name += name
    }
  },
  getters: {
    ageGetters(state) {
      // 会有代码自动补全!
      return state.age + 6
    }
  }
})


export const useSetup = defineStore('setup22',  () => {

  const perosn = ref('jyk')

  const setName = (name: string) => {
    perosn.value = name
  }

  const getName = () => {
    return perosn.value + '函数获取'
  }

  return {
    perosn,
    setName,
    getName
  }
})