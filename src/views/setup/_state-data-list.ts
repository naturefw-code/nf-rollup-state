
import type { InjectionKey } from 'vue'
import { reactive, ref, readonly, watch } from 'vue'

import {
  defineStore,
  BaseArray,
  defineState,
  useList,
  useModel
} from '../../../lib/main'
 


// 引入接口
import type { IListWebInfo, IQuery, IPagerInfo, ICompany } from '../../types/type'

 
// 区分局部状态的标记
const flag = Symbol('pager') // as InjectionKey<string>

// 泛型约束，回调函数返回的类型
type Res = {
  allCount: number, 
  list: Array<ICompany>
}

let getList

/**
 * 注册局部状态，数据列表用
 * @param service 获取数据的回调函数
 * @returns 
 */
export function useListState<T extends Res>(
    service: (query: IQuery, pagerInfo: IPagerInfo) => Promise<T>
  ) {
   
  // 定义列表用 的状态
  // const state = defineStore<IListWebInfo<ICompany>>(flag,
  const useState = () => {
      // 是否需要加载数据
      let canLoad = true

      // 页面信息
      const meta = reactive({
        title: '列表测试', // 模块标题
        deep: 0, // 递归层数
        moduleId: 100 // 模块ID
      })
      // 记录集合，reactive
      const dataList = useList<ICompany>([])
      // 查询条件
      const query = reactive({
        findValue: {}, // 查询条件的精简形式
        findArray: [], // 查询条件的对象形式
      })
      // 分页相关的信息
      const pagerInfo = reactive({
        pagerSize: 5,
        count: 20, // 总数
        pagerIndex: 0 // 当前页号
      })
      const selection = reactive({
        dataId: '',
        row: useModel<ICompany>(() => {
          return {
            id: '',
            name: '',
            address: '',
            telephone: ''
          }
        }),
        dataIds: useList([]),
        rows: useList([])
      })
      /**
       * 加载数据的函数，
       * @param isReset true：设置总数，页号设置为1；false：仅翻页
      */
      async function loadData (isReset = false) {
        if (isReset) {
          canLoad = false
          pagerInfo.pagerIndex = 1
        }
        
        // 获取数据
        const { allCount, list } = await service(query, pagerInfo)
        
        pagerInfo.count = allCount
        dataList.$state = list
      }

      // 监听页号，实现翻页功能
      watch(() => pagerInfo.pagerIndex, () => {
        if (canLoad) {
          loadData()
        } else {
          canLoad = true
        }
      })

      // 监听查询条件，实现查询功能。findValue
      watch(query.findArray, () => {
        loadData(true)
      })
      // 返回一个状态，如果需要只读，可以使用 readonly 
      return {
        loadData, // 其实是内部使用的。
        meta,
        dataList,
        selection,
        pagerInfo, // 分页信息
        query // 查询条件
      }
    }
  
  const state = useState()
  // 初始化
  state.loadData(true)

  const [ regList, getList ] = defineState(state)

  getList = getList
  // 返回 数据和状态
  return {
    regList, getList
  }
}

export function getListState() {
  return getList
}