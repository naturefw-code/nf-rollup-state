
import type { InjectionKey } from 'vue'
import { reactive, ref, readonly, watch } from 'vue'

import {
  defineStore,
  useStoreLocal,
  BaseArray,
  BaseObject,
  List,
  Model
} from '../../../lib/main'
 
// 引入接口
import type { ICompany, IGridSelection } from '../../types/type'
 
// 区分局部状态的标记
const flag = Symbol('data-select') // as InjectionKey<string>
/**
 * 注册局部状态，数据列表用
 * @param service 获取数据的回调函数
 * @returns 
 */
export function regDataSelectState() {
  // 用户选择的记录
  
  const selection = reactive({
    dataId: '',
    row: Model<ICompany>(() => {
      return {
        id: '',
        name: '',
        address: '',
        telephone: ''
      }
    }),
    dataIds: List([]),
    rows: List([]),
  })

  // 定义列表用 的状态
  const state = defineStore<IGridSelection<ICompany>>(flag, selection)
   
  // 返回 数据和状态
  return state 
}

/**
 * 子组件用 inject 获取状态
 * @returns
 */
export const getDataSelectState = ()  => {
  return useStoreLocal<IGridSelection<ICompany>>(flag)
}
 
