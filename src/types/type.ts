import type { Ref } from 'vue'
import {
  useList,
  useModel
} from '../../lib/main'

import type {
  BaseArray,
  BaseObject
} from '../../lib/main'

/**
 * 测试一下
 */
export interface Person {
  isLogin: boolean,
  name: string, //
  age: number,
  info: {
    a1: string,
    b: {
      b1: string
    }
  } 
}
  

// 演示用
export interface ICompany {
  id: string,
  name: string,
  address: string,
  telephone: string
}

/**
 * 分页信息
 */
export interface IPagerInfo {
  pagerSize: number,
  count: number, // 总数
  pagerIndex: number // 当前页号
}

/**
 * 列表里选择的记录
 */
export interface ISelection {
  dataId: string | number, // 单选ID number 、string
  row: BaseObject<object>, // 单选的数据对象 {}
  dataIds: BaseArray<string | number>, // 多选ID []
  rows: BaseArray<object> // 多选的数据对象 []
}

/**
 * 列表里选择的记录
 */
export interface IGridSelection<T extends object> {
  dataId: string | number, // 单选ID number 、string
  row: BaseObject<T> & T, // 单选的数据对象 {}
  dataIds: BaseArray<string | number>, // 多选ID []
  rows: BaseArray<T> // 多选的数据对象 []
}

/**
 * 查询条件
 */
export interface IQuery {
  findValue: any, // 查询条件的精简形式
  findArray: Array<{key:string, value:string}>, // 查询条件的对象形式
}

/**
 * 列表页面用的状态
 */
export interface IListWebInfo<T extends object> {
  meta: {
    title: string, // 模块标题
    deep: number, // 递归层数
    moduleId: string // 模块ID
  },
  dataList: BaseArray<T>, // 数据列表
  selection: IGridSelection<T>, // 选择
  query: IQuery, // 查询条件
  pagerInfo: IPagerInfo, // 分页信息
  // 加载数据
  loadData: (isReset: boolean) => void
}