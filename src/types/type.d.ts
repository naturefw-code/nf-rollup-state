import type { BaseArray, BaseObject } from '../../lib/main';
/**
 * 测试一下
 */
export interface Person {
    isLogin: boolean;
    name: string;
    age: number;
    info: {
        a1: string;
        b: {
            b1: string;
        };
    };
    arr: [
        {
            c: {
                c1: string;
            };
            d: {
                d1: string;
            };
        }
    ];
    roles: Array<string>;
    test?(): void;
}
/**
 * 只有 state 的状态 的测试
 */
export interface StateOnly {
    name: string;
}
/**
 * 普通状态的测试
 */
export interface StateGA {
    name: string;
    age: number;
    [key: number | string]: any;
}
export interface ICompany {
    id: string;
    name: string;
    address: string;
    telephone: string;
}
/**
 * 分页信息
 */
export interface IPagerInfo {
    pagerSize: number;
    count: number;
    pagerIndex: number;
}
/**
 * 列表里选择的记录
 */
export interface ISelection {
    dataId: string | number;
    row: BaseObject<object>;
    dataIds: BaseArray<string | number>;
    rows: BaseArray<object>;
}
/**
 * 列表里选择的记录
 */
export interface IGridSelection<T extends object> {
    dataId: string | number;
    row: BaseObject<T> & T;
    dataIds: BaseArray<string | number>;
    rows: BaseArray<T>;
}
/**
 * 查询条件
 */
export interface IQuery {
    findValue: any;
    findArray: Array<object>;
}
/**
 * 列表页面用的状态
 */
export interface IListWebInfo<T extends object> {
    meta: {
        title: string;
        deep: number;
        moduleId: string;
    };
    dataList: BaseArray<T>;
    selection: IGridSelection<T>;
    query: IQuery;
    pagerInfo: IPagerInfo;
    loadData: (isReset: boolean) => void;
}
