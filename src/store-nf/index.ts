 
// 加载状态的类库
// import { defineStore } from '../../lib/main'

// 加载用户状态
import regUserState from './state-user'

console.log('/src/store-nf/index.ts 被加载了！')

// 创建用户状态
const userState = regUserState()

// 记入全局状态
const store = {
  userState
}

// 变成全局状态
export default store

