
// type InjectionKey, 
import { reactive, readonly } from 'vue'

export type User = {
  user: {
    name1: string
    isLogin: boolean
    power: {
      modules: Array<number | string>
    }
  },
  login: () => void,
  logout: () => void

}

/**
 * 注册局部的当前登录用户的状态
 * @returns 用户状态和操作方式
 */
export default function regUserState() {
  // console.log('regUserState 被调用了！')

  // 定义内部成员
  // 内部使用的用户状态
  const user = reactive({
    name: '没有登录',
    isLogin: false,
    power: {
      modules: [] as Array<number | string>
    }
  })

  // 登录用的函数，仅示意，不涉及细节
  const login = () => {
    // 模拟登录
    user.name = '张三'
    user.isLogin = true
    user.power.modules.length = 0
    user.power.modules = [...[1,2,3]]
  }

  // 模拟退出，仅示意
  const logout = () => {
    // 模拟退出
    user.name = '已经退出'
    user.isLogin = false
    user.power.modules.length = 0
  }
 
  // 返回 数据和状态
  return {
    user: readonly(user),
    // ...toRefs(readonly(user)),
    login,
    logout,
  }
}
 
