import { Document, FolderOpened } from '@element-plus/icons-vue'
import { createRouter } from '@naturefw/ui-elp'

import home from '../views/home.vue'

// import list from '../views/data-list.vue'

// import list from '../views/plat/p02-table.vue'

/**
 * 基类的测试
 * * 对象、数组、综合
 * option 风格的状态
 * setup风格的状态
 * 全局状态
 * 局部状态
 * 
 */

const baseUrl = (document.location.host.includes('.gitee.io')) ?
  '/nf-rollup-state' :  ''

export default createRouter({
  /**
   * 基础路径
   */
  baseUrl: baseUrl,
  /**
   * 首页
   */
  home: home,

  menus: [
    {
      menuId: '100',
      title: '基础 class',
      naviId: '0',
      path: 'class',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '110',
          title: 'base-object',
          path: 'object',
          icon: Document,
          component: () => import('../views/base/10-base-object.vue')
        },
        {
          menuId: '120',
          title: 'base-array',
          path: 'array',
          icon: Document,
          component: () => import('../views/base/40-base-array.vue')
        },
        {
          menuId: '130',
          title: 'base-model',
          path: 'model',
          icon: Document,
          component: () => import('../views/base/20-base-model.vue')
        },
        {
          menuId: '160',
          title: '迭代',
          path: 'iterator',
          icon: Document,
          component: () => import('../views/base/60-iterator.vue')
        }
      ]
    },
    {
      menuId: '200',
      title: '基础状态',
      naviId: '0',
      path: 'base-state',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '220',
          title: '简单状态',
          path: 'state',
          icon: Document,
          component: () => import('../views/state/10-state.vue')
        },
        {
          menuId: '230',
          title: '表单的Model',
          path: 'model',
          icon: Document,
          component: () => import('../views/state/20-model.vue')
        },
        {
          menuId: '250',
          title: '数组',
          path: 'model',
          icon: Document,
          component: () => import('../views/state/40-list.vue')
        },
      ]
    },
    {
      menuId: '400',
      title: '全局状态',
      naviId: '0',
      path: 'global',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '440',
          title: 'user',
          path: 'user',
          icon: Document,
          component: () => import('../views/global/user.vue')
        },
      ]
    },
    {
      menuId: '10000',
      title: '列表',
      naviId: '0',
      path: 'list',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '10010',
          title: '基本用法',
          path: 'index',
          icon: Document,
          component: () => import('../views/list/_base.vue')
        },
        {
          menuId: '10020',
          title: '增删改查',
          path: 'crud',
          icon: Document,
          component: () => import('../views/list/_crud.vue')
        },
       
      ]
    },
    {
      menuId: '8000',
      title: 'pinia',
      naviId: '0',
      path: 'pinia',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '8010',
          title: '试一试',
          path: 'test',
          icon: Document,
          component: () => import('../views/pinia/pinia.vue')
        },
        {
          menuId: '8020',
          title: '定位代码',
          path: 'logcode',
          icon: Document,
          component: () => import('../views/pinia/pinia_log.vue')
        },
        {
          menuId: '8030',
          title: 'setup定位',
          path: 'logcodesetup',
          icon: Document,
          component: () => import('../views/pinia/pinia_log_setup.vue')
        }
      ]
    },
    {
      menuId: '2000',
      title: 'vue3.5',
      naviId: '0',
      path: 'vue3.5',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '2010',
          title: 'watch',
          path: 'watch',
          icon: Document,
          component: () => import('../views/watch/watch1.vue')
        },
        {
          menuId: '2015',
          title: 'ref的数组',
          path: 'ref1',
          icon: Document,
          component: () => import('../views/watch/ref-array.vue')
        },
        {
          menuId: '2016',
          title: 'ref的对象',
          path: 'ref2',
          icon: Document,
          component: () => import('../views/watch/ref-object.vue')
        },
        {
          menuId: '2020',
          title: 'onWatcherCleanup',
          path: 'onWatcherCleanup',
          icon: Document,
          component: () => import('../views/watch/cleanup.vue')
        },
        {
          menuId: '2030',
          title: 'defineModel',
          path: 'base',
          icon: Document,
          component: () => import('../views/watch/model/index.vue')
        } 
      ]
    },
  ]
})
