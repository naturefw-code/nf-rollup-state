
import type {
  TStateKey,
  TAnyObject,
  IState,
  IList,
  IModel,
} from './type'

export {
  TStateKey,
  TAnyObject,
  IState,
  IList,
  IModel,
} from './type'

/**
 * 给对象加上辅助功能：$state、$toRaw
 * @param obj 初始值， 对象
 * @param id 标识，记录日志用。[id=Symbol('_object')] - 对象的唯一标识符，默认为Symbol('_object')。
 */
export class BaseObject<T extends TAnyObject> implements IState<T> {
  #private;
  /**
   * 创建一个基础的对象，实现辅助工具的功能
   * @param obj 初始值，可以是对象，也可以是函数
   * @param id 标识，记录日志用
   */
  constructor(obj: T, id?: TStateKey);
  /**
   * 整体赋值。
   * 定义一个名为 $state 的 setter 方法，用于设置当前状态对象的值。
   */
  set $state(value: T);
  /**
   * 设置部分属性
   */
  $patch<T2 extends T>(value: T2): void;
  /**
   * 取原型，去掉内部方法
  */
  $toRaw<T extends TAnyObject>(): T;
  /**
   * 获取标识，string | symbol
   */
  get $id(): TStateKey;
}

/**
 * 继承 Array 实现 IState 接口，实现辅助功能
 * @param arr 初始值，数组
 * @param id 标识
 */
export class BaseArray<T> extends Array implements IList<T> {
  #private;
  constructor(arr: Array<T> | T, id?: TStateKey);
  /**
   * 没有实现功能，仅兼容 对象基类的方法
   */
  $patch(): void;
  /**
   * 整体替换，会清空原数组，
   */
  set $state(value: Array<T>);
  /**
   * 兼容 .valeue 的风格，整体赋值
   */
  set value(value: Array<T>);
  /**
   * 兼容 .value 的风格，返回数组
   */
  get value(): Array<T>;
  /**
   * 取原型，不包含内部方法，不维持响应性
   */
  $toRaw<T>(): Array<T>;
  /**
   * 获取ID
   */
  get $id(): TStateKey;
  /**
   * 在开头添加，封装 unshift
   * @returns 返回新数组的长度
   */
  $pushA(...arg: Array<any>): number;
  /**
   * 在指定位置i开始添加新元素，封装 splice
   * @param i 从 0 开始的位置
   * @param val 要添加的新元素
   * @returns 返回新数组的长度
   */
  $pushAt(i: number, ...arg: Array<any>): void;
  /**
   * 删除第一个元素
   * @returns 返回被删除的元素
   */
  $deleteA(): void;
  /**
   * 删除从指定位置 i 开始的 n 个元素，返回被删掉的函数
   * @param i 从 0 开始的位置
   * @param n 删除多少个元素
   * @returns 返回被删除的元素
   */
  $deleteAt<T>(i: number, n: number): unknown[];
  /**
   * 删除最后一个元素，封装 pop
   * @returns 返回被删除的元素
   */
  $deleteZ(): void;
  /**
   * 交换两个数组元素的位置
   * @param i1 数组下标
   * @param i2 数组下标
   */
  $swap(i1: number, i2: number): void;
}

/**
 * 表单的model，加上 $reset 功能
 * @param fun 初始值，函数，方便实现重置的功能
 * @param id 标识，记录日志用
 */
export class BaseModel<T extends TAnyObject> extends BaseObject<T> implements IModel<T> {
  #private;
  constructor(fun: () => T, id?: TStateKey);
  /**
   * 恢复初始值，值支持浅层
   */
  $reset(): void;
  /**
   * 新 model 替换旧 model，delete 原有属性，set 新属性
   */
  set $set(value: TAnyObject);
}

/**
 * 给 baseObject 套上 reactive，算是一个工厂吧
 * @param obj 对象
 * @returns
 */
export function useState<T extends TAnyObject>(obj: T): T & IState<T>;

/**
 * 给 BaseArray 套个壳，加上 reactive 实现响应性 & IState
 * @param val 数组或者函数
 * @param deep 是否深层响应。true：基于 reactive；false：使用 shallowRef
 * @param id 标识
 * @returns
 */
export function useList<T>(val: Array<T>, id?: TStateKey): BaseArray<T> & T;

/**
 * 给 BaseModel 套上 reactive，合并类型
 * @param fun 用函数的方式设置初始值，方便实现 reset
 * @returns
 */
export function useModel<T extends TAnyObject>(fun: () => T): T & IModel<T>;

/**
 * 代码定位，获得修改属性的代码位置。
 * 仅支持开发模式。
 * @param ret 要监听的对象，必须是 reactive。
 */
export function lineCode(ret: any): void;
