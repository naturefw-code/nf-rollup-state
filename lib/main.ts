// 引入各种函数，便于做成npm包

// 注册和获取局部状态，基于 依赖注入 封装
// import defineState from './state3/defineState'

// 创建全局状态， 获取全局状态
// import {defineStore, useStore} from './state3/defineStore'

// 深层拷贝
// import { deepSet, deepClone } from './base/assign'

//import {
//  addLog, // 记录容器
//  writeLog // 添加一条记录
//} from './base/_log'

// 安装插件
// import nfState from './state3/nfState'

// 数组基类
import BaseArray from './state/base-array'
// 对象基类
import BaseObject from './state/base-object'
// 表单基类
import BaseModel from './state/base-model'

// 工厂
import useModel from './state/create-model'
import useState from './state/create-state'
// 可以给数据列表用
import useList from './state/create-array'

// 代码定位
import lineCode from './state/line-code'

export {
  // 日志
  lineCode, // 代码定位
  // 基类
  BaseArray, // 数组基类
  BaseObject, // 对象基类
  BaseModel, // 表单基类
  // 创建状态
  useState, // 语法糖，创建对象
  useModel, // 语法糖，创建表单
  useList // 语法糖，创建数组
}

  // 安装插件
  // nfState,
  // addLog, // 记录容器
  // writeLog, // 添加一条记录
  // 拷贝
  // deepSet,
  // deepClone,
  // 状态
  // defineStore, // 统一创建全局状态
  // useStore, // 获取全局状态，
  // defineState // 注册和获取局部状态