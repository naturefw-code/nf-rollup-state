
/**
 * 安装插件，在main里面，template 里面可以使用 $state 获取全局状态
 */

// 获取类型
// import type { InjectionKey } from 'vue'

import { useStore } from './defineStore'
// export const _storeFlag = Symbol('__nf-state__') as InjectionKey<IStore>

/**
 * 安装插件 
 */
export default function nfState(app: any) {
  const store = useStore()
  // 
  // 设置模板直接使用状态
  app.config.globalProperties.$state = store
  // 发现个问题，这是个object，不用注入到 provide
  // app.provide(_storeFlag, store)
}