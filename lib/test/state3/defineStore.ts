/**
 * 注册全局状态
 */

import { readonly } from 'vue'

import type { TAnyObject } from '../types/type'

// 定义一个 全局的容器

const store = {} as TAnyObject

/**
 * 统一注册全局状态
 * @param stateList 全局状态集合，对象、reactive。不支持基础类型
 */
export function defineStore<T extends TAnyObject>(stateList: T) {

  // 注册全局状态
  Object.keys(stateList).forEach(key => {
    store[key] = stateList[key]
  })
 
}

/**
 * 获取全局状态，只读
 * @returns 全局状态
 */
export function useStore() {
  return readonly(store)
}