
import { computed, reactive, markRaw } from 'vue'
// import type { InjectionKey } from 'vue'
import type {
  TStateKey,
  TAnyObject,
  IModel,
  TObjectOrFunction,
  IStateOption
} from '../types/type'

// import BaseObject from './base-object'
import Model from './base-model'

export type _Method = (...args: any[]) => any
export type _ActionsTree = Record<string, _Method>

export interface IStateCreateOption2<T> {
  state: TObjectOrFunction;
  getters?: T | TAnyObject;
  actions?: _ActionsTree;
  options?: IStateOption;
}

/**
 * 传入参数，创建有getter、actions 的状态，reactive
 * @param id 状态的标志
 * @param info StateCreateOption state、getter、action 
 * * state：状态：函数
 * * getters?：变成 computed 的对象集合
 * * actions?: 变成 action 的对象集合
 */
export default function createAction<T extends TAnyObject>(
    id: TStateKey = Symbol('_action'),
    info: IStateCreateOption2<T>,
  ) {

  // 创建实例
  const tmp = new Model<T>(info.state as () => T, id)
  // 套上 reactive 
  const ret  = reactive(tmp)
 
  // 挂载 getters，变成 computed
  if (typeof info.getters === 'object') {
    Object.keys(info.getters).forEach(key => {
      // 在实例上面挂载 computed
      Object.defineProperty(tmp, key, {
        value: markRaw(computed(() => {
          const re = (info.getters as TAnyObject)[key].call(ret, ret)
          return re
        })),
        writable: true
      })
    })
  }

  // 挂载 actions
  if (typeof info.actions === 'object') {
    Object.keys(info.actions).forEach(key => {
      // 在实例上面挂载 action
      Object.defineProperty(tmp, key, {
        value: async function (...arg: Array<any>) {
          const fun = (info.actions as TAnyObject)[key]
          if (fun.toString().match(/^\(.*\) => \{/)) {
            // 箭头函数
            await (info.actions as TAnyObject)[key].call(ret, ret, ...arg)
          } else {
            // 普通函数
            await (info.actions as TAnyObject)[key].call(ret, ...arg)
          }
        },
        writable: true
      })
 
    })
  }

  // type Getters = Partial<IStateCreateOption>
  // type Getters2 = Pick<IStateCreateOption, 'getters'>
  // type Getters3 = typeof info.getters

  // type Getters = Partial<_ActionsTree>
  
  return ret  as T & IModel<T> // & Getters
}