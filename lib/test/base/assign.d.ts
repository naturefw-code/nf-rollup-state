declare type myObject = {
    [key: string]: any;
};
/**
 * 深层设置属性值，保留原属性，然后覆盖新属性值 options
 * @param target 目标
 * @param _source 来源
 */
export declare function deepSet(target: myObject, _source: myObject): myObject;
/**
 * 深层拷贝，只拷贝数据，新对象
 * @param target 目标，空的
 * @param  _source 来源
 */
export declare function deepClone(target: myObject, _source: myObject): myObject;
/**
 * 以 target 的属性为准，进行赋值。支持部分深层copy
 * * 如果属性是数组的话，可以保持响应性，但是不支持深层copy
 * * 如果属性是对象的话，可以支持深考
 * * 如果 有 $state，会调用。
 * @param target 目标
 * @param source 源
 */
export declare const copy: (target: myObject, source: myObject) => void;
export {};
