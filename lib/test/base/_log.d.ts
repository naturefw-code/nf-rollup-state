import type { IAnyObject, IStateLog, IStateKey } from '../types/type';
/**
 * 记录状态的变化日志，用key来区分
 * * stateLog = {
 * *  key: {
 * *   log: [
 * *     {
 * *       time: '时间戳',
 * *       kind: '', // 操作类型
 * *       oldValue: {},
 * *       newValue: {},
 * *       subValue: {}, // 参数
 * *       callFun: '' // 调用的函数名
 * *     }
 * *   ]
 * *  }
 * * }
 */
declare const stateLog: IStateLog;
/**
  * 添加一个新记录
  * @param key 状态的key
  * @param kind 操作类型
  * @param oldVal 原值，序列化
  * @param newVal 新值
  * @param subVal 参数，比如 patch
  * @param _stackstr stack 拆分为数组后，记录哪个元素
  */
declare function addLog(key: IStateKey, kind: string, oldVal: string, newVal: IAnyObject, subVal?: IAnyObject, _stackstr?: string): void;
/**
 * 写日志的语法糖
 * @param me 状态，this
 * @param submitVal 触发改变的值
 * @param kind 操作类型
 * @param index 日志的位置
 * @param callback 回调函数
 */
declare const writeLog: (me: IAnyObject, submitVal: IAnyObject, kind: string, index: number, callback: () => void) => Promise<void>;
export { writeLog, // 写一条日志
stateLog, // 记录容器
addLog };
