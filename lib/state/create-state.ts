
import { reactive } from 'vue'
import BaseObject from './base-object'
import type {
  TAnyObject,
  IState
} from '../types/type'

/**
 * 给 baseObject 套上 reactive，算是一个工厂吧
 * @param obj 对象
 * @returns 
 */
export default function useState<T extends TAnyObject> (
  obj: T
): T & IState<T> {
  const obj1 = new BaseObject(obj)
  return reactive(obj1) as T & IState<T>
}