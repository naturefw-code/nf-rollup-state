import { reactive } from 'vue'
import BaseModel from './base-model'

import type {
  TAnyObject,
  IModel
} from '../types/type'

/**
 * 给 BaseModel 套上 reactive，合并类型
 * @param fun 用函数的方式设置初始值，方便实现 reset
 * @returns 
 */
export default function useModel<T extends TAnyObject> (
  fun: () => T,
): T & IModel<T> {
  const obj1 = new BaseModel<T>(fun)
  return reactive(obj1) as T & IModel<T>
}