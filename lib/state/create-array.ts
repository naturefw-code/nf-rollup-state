
import BaseArray from './base-array'

import { reactive, shallowRef } from 'vue'

import type {
  TStateKey
} from '../types/type'

/**
 * 给 BaseArray 套个壳，加上 reactive 实现响应性 & IState
 * @param val 数组或者函数
 * @param deep 是否深层响应。true：基于 reactive；false：使用 shallowRef
 * @param id 标识
 * @returns 
 */
export default function useList<T>(
  val: Array<T>,
  id: TStateKey = Symbol('_array_reactive')
): BaseArray<T> & T {
  const re = new BaseArray<T>(val, id)
  const ret = reactive(re)
  return ret as BaseArray<T> & T
}