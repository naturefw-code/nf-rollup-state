
import { watch } from 'vue'
import type { DebuggerEvent } from 'vue'

/**
 * 代码定位，获得修改属性的代码位置。
 * 仅支持开发模式。
 * @param ret 要监听的对象，必须是 reactive。
 */
export default function lineCode(ret: any) {
  watch(ret, (newValue, oldValue) => {}, {
    onTrigger: (event: DebuggerEvent) => {
      // console.log('onTrigger', event)
      const err = new Error()
      if (err.stack){
        const arrayCode = err.stack.split('  at ')
        let codes = []
        // 寻找定位代码
        for(let i = 2; i < arrayCode.length; i++){
          if (!arrayCode[i].includes('/node_modules/')){
            codes.push(arrayCode[i])
          }
        }
        // 记录新旧值等
        const msg = {
          id:event.target?.$id.toString(), // 状态的id
          time: new Date().valueOf(), // 时间戳
          key: event.key, // 属性名
          oldValue: event.oldValue, // 旧值
          newValue: event.newValue, // 新值
          type: event.type, // 类型
          target: event.target, // 目标对象
          oldTarget: event.oldTarget, // 旧目标对象
          codeLine: codes[0],  // 触发变更的代码行

        }

        // 打印事件、新旧值等
        console.log(`\ntimeline，【${msg.id}】：\n`, msg)
        // 打印代码定位
        console.log(`\ntimeline，state 【${msg.id}】 mutations at：\n`, codes[0])
        // 打印error
        // console.log(`\ntimeline__${msg.id}__code：`, code)
      }
    }
  })
}