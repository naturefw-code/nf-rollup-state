
/**
 * 对象 key 的 类型
 */
export type TStateKey = string | number | symbol

/**
 * 任意一个对象
 */
export type TAnyObject = {[key: TStateKey]: any}

/**
 * 基础类型，整体赋值，取原型，是否开启代码定位
 */
export interface IState<T> {
  /**
   * 状态标识，string | symbol
   */
  get $id(): TStateKey;
  /**
   * 整体赋值
   */
  set $state(value: T | Array<T>);
  /**
   * 修改部分属性
   */
  $patch(_val: TAnyObject): void;
  /**
   * 取原型，去掉内部方法
   */
  $toRaw<T extends TAnyObject>(): T | Array<T>;

}

/**
 * 数组用的，兼容 ref 的.value 的用法
 */
export interface IList<T> extends IState<T> {
  /**
   * 兼容 ref 的 .value 的用法
   */
  set value(value: Array<T>);
  /**
   * 兼容 ref 的 .value 的用法
   */
  get value(): Array<T>;
}

/**
 * 表单里面 model，增加重置，需要传入函数作为初始值
 */
export interface IModel<T> extends IState<T> {
  /**
   * 重置，恢复初始值。
   */
  $reset(): void;

  /**
   * 新 model 替换旧 model，delete 原有属性，set 新属性
   */
  set $set(value: TAnyObject);

 
}