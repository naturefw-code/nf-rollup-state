
import type {
  IStoreKey,
  IStateKey,
  IAnyObject,
  IAnyArray,
  IAnyFunction,
  IAnyFunctionObject,
  IAnyFunctionArray,
  IObjectOrFunction,
  IArrayOrFunction,
  IObjectOrArray,
  IStore,
  IState,
  IStateLogInfo,
  IStateLog,
  IStateOption,
  IStateCreateOption,
  IStateCreateListOption
} from './type'

export {
  IStoreKey,
  IStateKey,
  IAnyObject,
  IAnyArray,
  IAnyFunction,
  IAnyFunctionObject,
  IAnyFunctionArray,
  IObjectOrFunction,
  IArrayOrFunction,
  IObjectOrArray,
  IStore,
  IState,
  IStateLogInfo,
  IStateLog,
  IStateOption,
  IStateCreateOption,
  IStateCreateListOption
} from './type'

/**
 * 给对象加上辅助功能：$state、$patch、$reset
 * @param objOrFunction 初始值，可以是对象，也可以是函数
 * @param id 标识，记录日志用
 * @param isLog 是否记录日志
 */
export declare class BaseObject<T extends IAnyObject> implements IState {
  #private;
  /**
   * 创建一个基础的对象，实现辅助工具的功能
   * @param objOrFunction 初始值，可以是对象，也可以是函数
   * @param id 标识，记录日志用
   * @param isLog 是否记录日志
   */
  constructor(objOrFunction: T | (() => T), id?: IStateKey, isLog?: boolean);
  /**
   * 获取初始值，如果是函数的话，会调用函数返回结果
   */
  get $value(): T | (() => T);
  /**
   * 设置初始值
   */
  set $value(val: T | (() => T));
  /**
   * 恢复初始值，值支持单层
   */
  $reset(): void;
  /**
   * 设置新值
   */
  set $state(value: T);
  /**
   * 替换部分属性，只支持单层
   */
  $patch(obj: IAnyObject | ((val: T) => void)): Promise<void>;
  /**
   * 取原型，不包含内部方法
  */
  $toRaw<T extends IAnyObject>(): T;
  /**
   * 获取标识，string | symbol
   */
  get $id(): IStateKey;
  /**
   * 获取是否记录日志。true ：记日志；false： 不记日志（默认值）
   */
  get $isLog(): boolean;
  /**
   * 设置是否写日志
   */
  set $isLog(val: boolean);
  /**
   * 验证是不是充血实体类的状态
   */
  get $isState(): boolean;
  /**
   * 验证是不是有辅助工具，区分普通的对象
   */
  get $isObject(): boolean;
  /**
   * 验证是不是有辅助工具，区分普通的数组
   */
  get $isArray(): boolean;
  /**
   * 获取日志
   */
  get $logs(): import("./type").IStateLogInfo[];
  /**
   * 清空日志
   */
  $clearLog(): void;
}

/**
 * 继承 Array 实现 IState 接口，实现辅助功能
 */
export declare class BaseArray<T> extends Array implements IState {
  #private;
  /**
   * 数组的辅助工具
   * @param arrayOrFunction 初始值，数组或者函数
   * @param id 标识
   */
  constructor(arrayOrFunction: Array<T> | (() => Array<T>), id?: IStateKey);
  /**
   * 获取初始值
   */
  get $value(): T[];
  /**
   * 只支持函数形式的初始值
   */
  $reset(): void;
  /**
   * 没有实现功能，仅兼容 对象基类的方法
   */
  $patch(): void;
  /**
   * 整体替换，会清空原数组，
   */
  set $state(value: Array<T> | T);
  /**
   * 取原型，不包含内部方法，不维持响应性
   */
  $toRaw<T>(): Array<T>;
  /**
   * 获取ID
   */
  get $id(): IStateKey;
  /**
   * 获取是否记录日志
   */
  get $isLog(): boolean;
  /**
   * 获取是不是状态
   */
  get $isState(): boolean;
  /**
   * 获取是不是数组类型的状态
   */
  get $isArray(): boolean;
  /**
   * 获取是否对象类型的状态
   */
  get $isObject(): boolean;
  /**
   * 数组不记录日志
   */
  get $logs(): never[];
  /**
   * 清空日志
   */
  $clearLog(): void;
  /**
   * 在开头添加，封装 unshift
   * @returns 返回新数组的长度
   */
  $pushA(...arg: Array<any>): number;
  /**
   * 在指定位置i开始添加新元素，封装 splice
   * @param i 从 0 开始的位置
   * @param val 要添加的新元素
   * @returns 返回新数组的长度
   */
  $pushAt(i: number, ...arg: Array<any>): void;
  /**
   * 删除第一个元素
   * @returns 返回被删除的元素
   */
  $deleteA(): void;
  /**
   * 删除从指定位置 i 开始的 n 个元素，返回被删掉的函数
   * @param i 从 0 开始的位置
   * @param n 删除多少个元素
   * @returns 返回被删除的元素
   */
  $deleteAt<T>(i: number, n: number): unknown[];
  /**
   * 删除最后一个元素，封装 pop
   * @returns 返回被删除的元素
   */
  $deleteZ(): void;
  /**
   * 交换两个数组元素的位置
   * @param i1 数组下标
   * @param i2 数组下标
   */
  $swap(i1: number, i2: number): void;
}

/**
 * 传入参数，创建有getter、actions 的状态，reactive
 * @param id 状态的标志
 * @param info StateCreateOption state、getter、action、options
 * * state：状态：对象、数组，或者函数
 * * getters?：变成 computed 的对象集合
 * * actions?: 变成 action 的对象集合
 * * options?: 选项
 * * * isLocal —— true：局部状态；false：全局状态（默认属性）；
 * * * isLog ——  true：做记录；false：不用做记录（默认属性）；
 */
export declare function optionState<T extends IAnyObject>(id: IStateKey, info: IStateCreateOption): T & IState;

/**
 * 给 BaseObject 套个壳，加上 reactive 实现响应性。
 * @param fun 必须使用函数
 * @param id 标识，记录日志用
 * @param isLog 是否记录日志
 * @returns
 */
export declare function Model<T extends object>(fun: T | (() => T), id?: IStateKey, isLog?: boolean): BaseObject<T> & T;


/**
 * 给 BaseArray 套个壳，加上 reactive 实现响应性 & IState
 * @param val 数组或者函数
 * @returns
 */
export declare function List<T>(val: Array<T> | (() => Array<T>), id?: IStateKey): BaseArray<T> & T;
 
/**
 * 传入参数，创建有getter、actions 的状态，reactive
 * @param id 状态的标志
 * @param info StateCreateOption state、getter、action、options
 * * state：状态：对象、数组，或者函数
 * * getters?：变成 computed 的对象集合
 * * actions?: 变成 action 的对象集合
 * * options?: 选项
 * * * isLocal —— true：局部状态；false：全局状态（默认属性）；
 * * * isLog ——  true：做记录；false：不用做记录（默认属性）；
 */
export declare function OptionState<T>(id: IStateKey, info: IStateCreateOption): T & IState;
 
/**
 * 单独定义状态，可以是全局状态，也可以是局部状态
 * @param id 标识（string | symbol），全局状态不能重名；局部状态可以重名。
 * @param info 状态，四种情况
 * * info：
 * * 一：函数：setup 风格
 * * 二：reactive、readonly，直接存入状态
 * * 三：对象：含有 state 属性 -- option 风格
 * * 四：对象：无 state 属性 -- 直接视为 state，option 风格
 * @param isLocal Boolean 默认是局部状态
 */
export declare function defineStore<T extends IObjectOrArray>(id: symbol | string, info: IStateCreateOption | IAnyFunctionObject | IObjectOrArray, isLocal?: boolean): T;
/**
 * 保存局部状态的 BaseObject 类型
 * @param id 状态标识
 * @param state 状态，对象
 * @returns
 */
export declare function saveBaseObject<T extends IObjectOrArray>(id: symbol | string, state: T): BaseObject<T> & T;
/**
* 保存局部状态的 BaseObject 类型
* @param id 状态标识
* @param state 状态，数组
* @returns
*/
export declare function saveBaseArray<T>(id: symbol | string, state: T): BaseArray<T> & T;
/**
* 全局状态存入 store；局部状态存入 provide，返回状态
* @param id 状态标识
* @param isLocal 是否局部状态
* @param state 状态，对象或者数组
* @returns 返回状态
*/
export declare function save<T extends IObjectOrArray>(id: symbol | string, isLocal: boolean, state: T): (BaseObject<T> & T )| (BaseArray<T> & T) | (T & IState);


/**
 * 开局时创建一批全局状态。在main里面。
 * @param info 状态列表，多个状态，和回调函数
 * * store
 * * * state 的类型
 * * * * function：setup 风格，不记录日志，全局状态
 * * * * 对象：
 * * * * * 没有 state 属性：整个对象作为 state，无 getter、action
 * * * * * 有 state：option 风格
 * * init 创建完毕后的回调函数
 */
export declare function createStore(info: IStateCreateListOption): (app: any) => void;

/**
 * 获取全局状态。implements IState
 * @param id 全局状态 的 ID，string | symbol
 * @returns 指定的全局的状态
 */
export declare function useStore<T extends object>(id: string | symbol): T;
export declare function reBaseObject<T extends object>(state: any): T & BaseObject<T>;
export declare function reBaseArray<T>(state: any): T & BaseArray<T>;
/**
 * 获取局部状态，对象，setup
 * @param id 状态的 ID
 * @returns ID 对应的局部状态
 */
export declare function useStoreLocal<T extends object>(id: string | symbol): T;
/**
 * 获取局部状态，对象基类 BaseObject
 * @param id 状态的 ID
 * @returns ID 对应的局部状态
 */
export declare function useStoreLocalObject<T extends object>(id: string | symbol): T & BaseObject<T>;
/**
* 获取局部状态，数组，BaseArray
* @param id 状态的ID
* @returns 局部状态
*/
export declare function useStoreLocalArray<T extends object | any>(id: string | symbol): BaseArray<T>;

/**
 * 记录状态的变化日志，用key来区分
 * * stateLog = {
 * *  key: {
 * *   log: [
 * *     {
 * *       time: '时间戳',
 * *       kind: '', // 操作类型
 * *       oldValue: {},
 * *       newValue: {},
 * *       subValue: {}, // 参数
 * *       callFun: '' // 调用的函数名
 * *     }
 * *   ]
 * *  }
 * * }
 */
export declare const stateLog: IStateLog;

/**
  * 添加一个新记录
  * @param key 状态的key
  * @param kind 操作类型
  * @param oldVal 原值
  * @param newVal 新值
  * @param subVal 参数，比如 patch
  * @param _stackstr stack 拆分为数组后，记录哪个元素
  */
export declare function addLog(key: IStateKey, kind: string, oldVal: string, newVal: IAnyObject, subVal?: IAnyObject, _stackstr?: string): void;
/**
 * 写日志的语法糖
 * @param me 状态，this
 * @param submitVal 触发改变的值
 * @param kind 操作类型
 * @param index 日志的位置
 * @param callback 回调函数
 */
export declare const writeLog: (me: IAnyObject, submitVal: IAnyObject, kind: string, index: number, callback: () => void) => Promise<void>;
 